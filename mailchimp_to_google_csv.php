<?php

// PHP script to convert Mailchimp list export CSV file to Google Contacts CSV file
// by JR 9/7/2018
//
// USAGE:
// 1) Login to Mailchimp - Export the main list to a CSV
// 2) Unzip the zip file and rename the subscribed CSV to mailchimp_export.csv
// 3) Run this script :
//    php mailchimp_to_google_csv.php > google.csv
// 4) Login to Google Contacts - DELETE ALL CONTACTS or you can run Find & Merge Duplicates afterwards!
// 5) Import Contacts and import google.csv 
// 6) CLICK F5 to refresh and reload the 
// 7) Click the "imported DD/MM/YY" group - Delete it!
// 8) DONE!
//

$mailchimp_csv = "mailchimp_export.csv";
$google_csv = "google_test.csv";


// output the Google CSV header
$google_hdr = explode(",","Name,Given Name,Additional Name,Family Name,Yomi Name,Given Name Yomi,Additional Name Yomi,Family Name Yomi,Name Prefix,Name Suffix,Initials,Nickname,Short Name,Maiden Name,Birthday,Gender,Location,Billing Information,Directory Server,Mileage,Occupation,Hobby,Sensitivity,Priority,Subject,Notes,Group Membership,E-mail 1 - Type,E-mail 1 - Value,Phone 1 - Type,Phone 1 - Value");

foreach($google_hdr as $hdr) {
echo $hdr.",";
}
echo "\n";

// read mailchimp CSV file
$hdrs=array(); $dc=0; $row=0; 
if (($handle = fopen($mailchimp_csv, "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 5000, ",")) !== FALSE) {

		$row++;
		
		// read header
		if ($row==1) {
			foreach ($data as $d) { $hdrs[$d]=$dc++; }
			continue;
		}

		// ignore blank lines
		if ($data[0]=="") { continue; }

		// extract data values from mailchimp CSV
		$email = $data[$hdrs['Email Address']];
		$full_name = $data[$hdrs['First Name']] . " " . $data[$hdrs['Last Name']];
		$phone = $data[$hdrs['Phone Number']];
		$membership = explode(",",$data[$hdrs['Membership Type']]);

		// organise group membership into Google Contacts format
		$group_membership = "* My Contacts ::: All Members ::: ";
		if ($membership[0] != "") {
			foreach($membership as $category) {
				$group_membership .= trim($category). " ::: ";
			}
		}

		// plug mailchimp values into Google Contacts CSV format
		foreach($google_hdr as $hdr) {
			if ($hdr == "Name") { echo $full_name; }
			if ($hdr == "Group Membership") { echo $group_membership; }
			if ($hdr == "E-mail 1 - Type") { echo "* Other"; }
			if ($hdr == "E-mail 1 - Value") { echo $email; }
			if ($hdr == "Phone 1 - Type") { echo "Home";}
			if ($hdr == "Phone 1 - Value") { echo $phone; }

			echo ",";
		}

	echo "\n";
	//if ($row > 10) { die(""); } // stop after row 10
	}
    fclose($handle);
}
